const musisContainer = document.querySelector('.music-container');
const progressContainer = document.querySelector('.progress-container');
const timeRemaining = document.querySelector('#time-remaining');
const playBtn = document.querySelector('#play');
const prevBtn = document.querySelector('#prev');
const nextBtn = document.querySelector('#next');
const audio = document.querySelector('#audio');
const progress = document.querySelector('.progress');
const title = document.querySelector('#title');
const cover = document.querySelector('#cover');



const songs = ['hey', 'summer', 'ukulele'];

let songIndex = 0;

// load song into DOM
loadSong(songs[songIndex]);

// update song details
function loadSong(song) {
  title.innerText = song;
  audio.src = `music/${song}.mp3`;
  cover.src = `images/${song}.jpg`;
}

function playSong() {
  musisContainer.classList.add('play');
  playBtn.querySelector('i.fas').classList.remove('fa-play');
  playBtn.querySelector('i.fas').classList.add('fa-pause');

  audio.play();
}

function pauseSong() {
  musisContainer.classList.remove('play');
  playBtn.querySelector('i.fas').classList.add('fa-play');
  playBtn.querySelector('i.fas').classList.remove('fa-pause');

  audio.pause();
}

function prevSong() {
  songIndex--
  if(songIndex < 0) {
    songIndex = songs.length -1 
  }
  loadSong(songs[songIndex]);
  playSong();
}

function nextSong() {
  songIndex++
  if(songIndex > songs.length -1) {
    songIndex = 0
  }
  loadSong(songs[songIndex]);
  playSong();
}

function updateProgress(e) {
  const { duration, currentTime } = e.srcElement;
  const progressPercent = (currentTime / duration) * 100;
  const remaining = Math.floor(duration - currentTime) || 0;
  progress.style.width = `${progressPercent}%`;
  timeRemaining.innerText = toHHMMSS(remaining);
}

function setProgress(e) {
  const width = this.clientWidth;
  const clickX = e.offsetX;
  const duration = audio.duration;

  audio.currentTime = (clickX / width) * duration;
}

function toHHMMSS(totalSeconds) {
  let hours = Math.floor(totalSeconds / 3600);
  let minutes = Math.floor(totalSeconds / 60) % 60;
  let seconds = totalSeconds % 60;

  // additional formating
  minutes = String(minutes).padStart(2, "0");
  hours = String(hours).padStart(2, "0");
  seconds = String(seconds).padStart(2, "0");

  return `${hours}:${minutes}:${seconds}`;
}


// event listeners
playBtn.addEventListener('click', () => {
  const isPlaying = musisContainer.classList.contains('play');
  if(isPlaying) {
    pauseSong();
  } else {
    playSong();
  }
})


prevBtn.addEventListener('click', prevSong);
nextBtn.addEventListener('click', nextSong);

audio.addEventListener('timeupdate', updateProgress);
progressContainer.addEventListener('click', setProgress);
audio.addEventListener('ended', nextSong);